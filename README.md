DEMS Manual 📖
==============

This tutorial will provide you with all the information that you need to setup the Digital Exam Management System (DEMS).

# Dependencies

* [Docker](setup/Docker.md)
* [Poetry](setup/Poetry.md)

