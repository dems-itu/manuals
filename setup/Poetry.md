Poetry Setup
============

Poetry is an easy-to-use Python packaging and dependency management tool.

This tutorial will explain how to install the poetry tool. You need it to install dependencies for apps in the DEMS project.

The best way for learning more about this tool is to use its [official webpage](https://python-poetry.org/).

# Install

Poetry documentation recommends using the install script provided in [this](https://python-poetry.org/docs/#installation) page. An alternate options are to use the `pip` package manager (`sudo pip install poetry`) or the one provided by your OS (package name usually is `python-poetry`).

# Usage

Poetry uses a `pyproject.toml` file as suggested by [PEP518](https://www.python.org/dev/peps/pep-0518/).

If the project has this file, you can execute the `poetry install` command and it will create a virtual environment and install the project dependencies in this virtual environment. After that, you can use `poetry run <actual_command>` commands in this directory. To activate this environment, you can use the `poetry shell` command. You don't need to add `poetry run` to your commands after activation the virtual environment.

Poetry installs both the required packages, and the development packages by default. To prevent it from install the development packages in the production, pass the `--no-dev` flag to the install command: `poetry install --no-dev`.

Check about other options using the help provided by the package (the `--help` flag).
