Docker Setup
============

Docker is used for containerization in the DEMS project for custom testing images.

This tutorial will explain how to setup the docker tool in your host machine (local or server).

# Install

First, you will need to install the docker package provided by your operating system's package manager. Check [this official installation page](https://docs.docker.com/engine/install/) for more details, but usually, the package name is `docker`.

# Setup permission

After installing the package, you need to add your current user to the docker group in order to be able to execute the docker command (to communicate with the docker daemon):

```bash
# create the docker group
$ sudo groupadd docker
# add user to the group, replace ${USER} with your username
$ sudo usermod -aG docker ${USER}
# re-evaluate your group membership. The suggested way is to logout and login, however, this should work too.
$ sudo su ${USER}
```

Now, check to make sure that you have access to the docker daemon by executing `docker ps` command.

If the daemon is not available, start it using service manager of your OS, for systemd use `sudo systemctl start docker` command.

Now you can use the docker command to build the images where it is necessary. Follow the instruction for each project for an optimal setup process.

# Current images

These images are currently built for the DEMS:
* [mastizada/demspytest](https://hub.docker.com/r/mastizada/demspytest) - Pytest image for the grading system.
* [mastizada/demspylint](https://hub.docker.com/r/mastizada/demspylint) - Pylint image for the syntax checking (flake8).

# Additional commands

* Remove all containers: `docker rm $(docker ps -qa)`
* Remove images: `docker rmi $(docker images -q -f dangling=true)`
* Remove all images: `docker rmi $(docker images -q)`
